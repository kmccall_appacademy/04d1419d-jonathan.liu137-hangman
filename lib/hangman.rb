require 'io/console' # required to hide human input

class Hangman
  attr_reader :guesser, :referee, :board, :strikes, :strike_count

  MAX_STRIKES = 7

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board = ""
    @strikes = []
  end

  def play
    setup
    display_hangman
    puts "Secret word: #{board}"
    puts "Strikes: #{strikes}"
    until over?
      take_turn
    end

    if strikes.size >= MAX_STRIKES
      puts "Game Over! The word was #{referee.secret_word}."
    else
      puts "You win! You guessed the word!"
    end
  end

  def setup
    word_length = referee.pick_secret_word
    guesser.register_secret_length(word_length)
    word_length.times { board << "_" }
    @graphics = [
      " |--------",
      " |     |  ",
      " |        ",
      " |        ",
      " |        ",
      " |        ",
      " |        "
    ]
  end

  def take_turn
    ch_guess = guesser.guess(board)
    indexes = referee.check_guess(ch_guess)
    update_board(ch_guess, indexes)
    guesser.handle_response(ch_guess, indexes)
  end

  def update_board(ch, index_array)
    if index_array.empty?
      strikes << ch
      @strike_count = strikes.size
      display_hangman
    else
      index_array.each { |i| @board[i] = ch }
    end

    puts "Secret word: #{board}"
    puts "Strikes: #{strikes}"
  end

  def display_hangman
    @graphics[2][7] = "O" if strike_count == 1
    @graphics[3][7] = "|" if strike_count == 2
    @graphics[4][7] = "|" if strike_count == 3
    @graphics[3][6] = "/" if strike_count == 4
    @graphics[3][8] = "\\" if strike_count == 5
    @graphics[5][6] = "/" if strike_count == 6
    @graphics[5][8] = "\\" if strike_count == 7
    puts @graphics
  end

  def over?
    strikes.size >= MAX_STRIKES || !board.include?('_') ? true : false
  end
end

class HumanPlayer
  attr_reader :secret_word

  def pick_secret_word
    puts "Please enter your secret word: "
    # hides input from other human players
    @secret_word = STDIN.noecho(&:gets).chomp
    secret_word.length
  end

  def register_secret_length(length)
    puts "The length of the secret word is #{length}."
  end

  def guess(_board, _strikes)
    ch_guess = ""
    until ('a'..'z').cover?(ch_guess) && !ch_guess.nil?
      print "Please guess a letter: "
      ch_guess = gets.chomp[0]

      puts "That is not a letter!" if !('a'..'z').cover?(ch_guess)
    end

    ch_guess.downcase
  end

  def check_guess(ch)
    if secret_word.include?(ch)
      puts "Good guess! The secret word contains the letter #{ch}.\n\n"
    else
      puts "Nope! The secret word does not contain the letter #{ch}."
    end

    secret_word.chars.each_index.select { |i| @secret_word[i] == ch }
  end

  def handle_response(_ch, _indexes) end
end

class ComputerPlayer
  attr_reader :secret_word, :dictionary, :candidate_words

  @@default_dictionary = File.readlines("lib/dictionary.txt")
  @@default_dictionary.map! { |word| word.delete("^a-z") }

  def initialize(word_list = @@default_dictionary)
    @dictionary = word_list
  end

  def pick_secret_word
    @secret_word = dictionary.sample
    secret_word.length
  end

  def register_secret_length(length)
    puts "The secret word is #{length} letters long."
    @candidate_words = dictionary.select { |word| word.size == length }
  end

  def guess(board)
    all_words = candidate_words.reduce(:+)
    (all_words.chars - board.chars).mode
  end

  def check_guess(ch)
    if secret_word.include?(ch)
      puts "Good guess! The secret word contains the letter #{ch}.\n\n"
    else
      puts "Nope! The secret word does not contain the letter #{ch}."
    end

    secret_word.chars.each_index.select { |i| secret_word[i] == ch }
  end

  def handle_response(ch, index_arr)
    if index_arr == []
      candidate_words.reject! { |word| word.include?(ch) }
    else
      candidate_words.select! do |word|
        word.chars.each_index.select { |i| word[i] == ch } == index_arr
      end
    end
  end
end

class Array

  def mode
    counter_hash = Hash.new(0)
    self.each { |el| counter_hash[el] += 1 }
    counter_hash.select! { |_k, v| v == counter_hash.values.max }
    counter_hash.keys.sample
  end
end


if $PROGRAM_NAME == __FILE__
  print "Would you like the guesser to be a computer? (y/n) "
  if gets.chomp[0].casecmp('y') == 0
    guesser = ComputerPlayer.new
  else
    guesser = HumanPlayer.new
  end

  print "Would you like the referee to be a computer? (y/n) "
  if gets.chomp[0].casecmp('y') == 0
    referee = ComputerPlayer.new
  else
    referee = HumanPlayer.new
  end

  Hangman.new(guesser: guesser, referee: referee).play
end
